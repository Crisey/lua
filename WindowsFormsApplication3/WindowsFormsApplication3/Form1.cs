﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLua;
using System.IO;
using System.Net;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        List<Button> buttonList = new List<Button>();
        List<Label> labelList = new List<Label>();
        List<TextBox> textBoxList = new List<TextBox>();
        List<CheckBox> checkBoxList = new List<CheckBox>();
        List<RadioButton> radioButtonList = new List<RadioButton>();
        List<PictureBox> pictureBoxList = new List<PictureBox>();
        List<ListView> listViewList = new List<ListView>();
        List<WebBrowser> webBrowserList = new List<WebBrowser>();
        List<ComboBox> comboBoxList = new List<ComboBox>();
        List<LinkLabel> linkLabelList = new List<LinkLabel>();
        List<ProgressBar> progressBarList = new List<ProgressBar>();
        List<GroupBox> groupBoxList = new List<GroupBox>();
        

        private readonly Lua lua = new Lua();

        public Form1()
        {
            this.BackgroundImageLayout = ImageLayout.Stretch;
            GC.Collect();

            InitializeComponent();
            lua.LoadCLRPackage();

            RegisterFunc("AddRange");               //Dodawanie dynamicznie wielkości listy
            RegisterFunc("AddToCtrl");              //Dodawanie do formy
            RegisterFunc("SetBackground");          //Dodawanie tła

            RegisterFunc("onClick");
            RegisterFunc("onDoubleClick");
            RegisterFunc("onMouseDown");
            RegisterFunc("onMouseUp");
            RegisterFunc("onMouseLeave");
            RegisterFunc("onMouseEnter");
            RegisterFunc("onMouseHover");

            lua["Button"] = buttonList;
            lua["Label"] = labelList;
            lua["CheckBox"] = checkBoxList;
            lua["RadioButton"] = radioButtonList;
            lua["PictureBox"] = pictureBoxList;
            lua["ListView"] = listViewList;
            lua["WebBrowser"] = webBrowserList;
            lua["TextBox"] = textBoxList;
            lua["ComboBox"] = comboBoxList;
            lua["LinkLabel"] = linkLabelList;
            lua["ProgressBar"] = progressBarList;
            lua["GroupBox"] = groupBoxList;

            try
            {
                lua.DoFile(@"scripts\Config.lua");

                Initialize();

                foreach (string st in Directory.GetFiles(@"scripts\", "*.lua", SearchOption.AllDirectories))
                {
                    if (st != @"scripts\Config.lua")
                        lua.DoFile(st);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void RegisterFunc(string name)
        {
            lua.RegisterFunction(name, this, this.GetType().GetMethod(name));
        }

        public void AddToCtrl(Control control)
        {
            this.Controls.Add(control);
            control = new Control();
        }

        public void onClick(Control control, Action action)
        {
            control.Click += (s, e) => { action(); };//run function from lua here or lua file
        }

        public void onDoubleClick(Control control, Action action)
        {
            control.DoubleClick += (s, e) => { action(); };
        }

        public void onMouseDown(Control control, Action action)
        {
            control.MouseDown += (s, e) => { action(); };
        }

        public void onMouseUp(Control control, Action action)
        {
            control.MouseUp += (s, e) => { action(); };
        }

        public void onMouseLeave(Control control, Action action)
        {
            control.MouseLeave += (s, e) => { action(); };
        }

        public void onMouseEnter(Control control, Action action)
        {
            control.MouseEnter += (s, e) => { action(); };
        }

        public void onMouseHover(Control control, Action action)
        {
            control.MouseHover += (s, e) => { action(); };
        }

        public void SetBackground(string link)
        {
            if (link.Contains("http"))
            {
                var request = WebRequest.Create(link);

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    this.BackgroundImage = Bitmap.FromStream(stream);
                }
            }
            else
            {
                this.BackgroundImage = Image.FromFile(link);
            }
        }

        public void AddRange<T>(List<T> list, int j)
             where T : new()
        {
            for (int i = 0; i < j; i++)
            {
                T obj = new T();
                list.Add(obj);
            }
        }

        public void Initialize()
        {
            var buttonCount = lua["buttonCount"] as double?;
            var labelCount = lua["labelCount"] as double?;
            var checkBoxCount = lua["checkBoxCount"] as double?;
            var radioButtonCount = lua["radioButtonCount"] as double?;
            var pictureBoxCount = lua["pictureBoxCount"] as double?;
            var listViewCount = lua["listViewCount"] as double?;
            var webBrowserCount = lua["webBrowserCount"] as double?;
            var textBoxCount = lua["textBoxCount"] as double?;
            var comboBoxCount = lua["comboBoxCount"] as double?;
            var linkLabelCount = lua["linkLabelCount"] as double?;
            var progressBarCount = lua["progressBarCount"] as double?;
            var groupBoxCount = lua["groupBoxCount"] as double?;

            AddRange(buttonList, (int)buttonCount);
            AddRange(labelList, (int)labelCount);
            AddRange(checkBoxList, (int)checkBoxCount);
            AddRange(radioButtonList, (int)radioButtonCount);
            AddRange(pictureBoxList, (int)pictureBoxCount);
            AddRange(listViewList, (int)listViewCount);
            AddRange(webBrowserList, (int)webBrowserCount);
            AddRange(textBoxList, (int)textBoxCount);
            AddRange(comboBoxList, (int)comboBoxCount);
            AddRange(linkLabelList, (int)linkLabelCount);
            AddRange(progressBarList, (int)progressBarCount);
            AddRange(groupBoxList, (int)groupBoxCount);
        }
    }
}

